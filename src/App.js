import { WeatherApp } from "./components/weatherapp";
import { useDispatch, useSelector } from "react-redux";

import "./App.css"
import { WeatherCity } from "./components/weatherCity";
function App() {
  const {weatherData} = useSelector((reduxData) =>
        reduxData.weatherReducer
    )
  return (
    
    <div className="body">
          {weatherData.message === 0 ? <WeatherCity/> : <WeatherApp/>}
    </div>
  );
}

export default App;
