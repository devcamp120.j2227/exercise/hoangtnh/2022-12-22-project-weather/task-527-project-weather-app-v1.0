import { ENTER_HANDLER, FETCH_API_ERROR, FETCH_API_SUCCESS, FETCH_API_SUCCESS_SUBMIT_CITY, FETCH_WEATHER_API, GET_INPUT_VALUE, SUBMIT_ON_WEATHER_CITY } from "../constants/weather.constants";

export const submitOnWeatherCity = (data) =>{
    return async (dispatch) =>{
        await dispatch({
            type: SUBMIT_ON_WEATHER_CITY,
            data: data
        })
    }
}
export const getInputValue = (data) =>{
    return async (dispatch) =>{
        await dispatch({
            type: GET_INPUT_VALUE,
            data: data
        })
    }
}
export const onEnterSubmit = (data) =>{
    return async (dispatch) =>{
        await dispatch({
            type: ENTER_HANDLER,
            data: data
        })
    }
}
export const fetchWeather = (city) =>{
     return async(dispatch) =>{
        var body = {
            method: 'GET',
            redirect: 'follow'
          };
          await dispatch({
            type: FETCH_WEATHER_API,
            data: city,
          });
          try {
            const response =  await fetch("https://api.openweathermap.org/data/2.5/forecast?q="+ `${city}` +"&exclude=daily&appid=a47744fc429cee4347e61fe489d939e5&units=metric", body);
            const data = await response.json();
            return dispatch({
                type: FETCH_API_SUCCESS,
                data: data,
            })
            
          } catch (error) {
            return dispatch({
                type: FETCH_API_ERROR,
                error: error
            })
          }
    }
}
export const fetchWeatherOnSubmit = (city) =>{
    return async(dispatch) =>{
        var body = {
            method: 'GET',
            redirect: 'follow'
            };
            await dispatch({
            type: FETCH_WEATHER_API,
            data: city,
            });
            try {
            const response =  await fetch("https://api.openweathermap.org/data/2.5/forecast?q="+ `${city}` +"&exclude=daily&appid=a47744fc429cee4347e61fe489d939e5&units=metric", body);
            const data = await response.json();
            return dispatch({
                type: FETCH_API_SUCCESS_SUBMIT_CITY,
                data: data,
            })
            
            } catch (error) {
            return dispatch({
                type: FETCH_API_ERROR,
                error: error
            })
            }
    }
}
        
        //     .then((data) =>{
        //             console.log(data);
        //             setInformation(data.list)
        //             //{data.message === 0 ? setCity("") : setCity(data.message); setStatus(false)}
        //             {data.message === 0 ? setStatus(true) : setStatus(false)}
        //         })
        //         .catch((error)=>{
        //             console.log(error.message);
        //             setCity("City not found, try again...");
        //         })
   
