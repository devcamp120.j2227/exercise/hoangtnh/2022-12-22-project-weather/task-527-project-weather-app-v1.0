import { Grid, TextField } from "@mui/material"
import { Box, Container } from "@mui/system";
import moment from "moment/moment";
import { useDispatch, useSelector } from "react-redux";
import { fetchWeatherOnSubmit, submitOnWeatherCity } from "../actions/weather.action";

export const WeatherCity = () =>{
    const onChangeHandler = (e)=>{
        dispatch(submitOnWeatherCity(e.target.value));
    }
    const dispatch = useDispatch();
    const {inputOnCityForm, weatherData, weatherDataOnCityForm} = useSelector((reduxData) =>
        reduxData.weatherReducer
    )
    const onEnter =(e)=>{         
        dispatch(submitOnWeatherCity(e.target.value));
        if(e.code === "Enter"){
            //lấy giá trị input sau đó in hoa chữ cái đầu và bỏ khoảng cách
            const upperCase = inputOnCityForm.charAt(0).toUpperCase() + inputOnCityForm.slice(1);
            const cityRemoveSpace = upperCase.replace(/\s/g,'');
            //method get weather                
                dispatch(fetchWeatherOnSubmit(cityRemoveSpace));
                dispatch(submitOnWeatherCity(weatherData));
            }
    }
    return (
        <Container xs={12} md={12} sm={12} lg={12}>
            {weatherData.cod === "200" ?
                <Box sx={{ display: "flex",
                        margin:"auto",
                        marginTop:"20%",
                        width: "90%",
                        minHeight: "500px",
                        boxShadow: "0px 0px 5px 9px #aed0eb",
                        borderRadius: "35px"
                        }}>

                        <Grid xs={12} md={12} sm={12} lg={12}
                            container
                            direction="col"
                            justifyContent="center"
                            alignItems="center"
                            textAlign="center"
                            > 
                            <Grid item xs={12} md={12} sm={12} lg={12} marginTop="-10%"> 
                                <TextField id="outlined-basic" label="Enter a city" variant="standard" 
                                InputLabelProps={{
                                    style : { color : '#365a7a', marginTop:"-2px", marginLeft:"20px"}
                                }} 
                                InputProps={{ 
                                    disableUnderline: true, style:{color:"#365a7a",paddingBottom:"10px", marginLeft:"20px"} 
                                }}
                                onKeyDown={onEnter} onChange={onChangeHandler} value={inputOnCityForm}
                                style={{width:"50%",backgroundColor: "white", borderRadius: 20}} className="inputRounded"/>
                            </Grid>
                            <Grid item xs={6} >
                                <img src= {`images/${weatherData.list[0].weather[0].icon}.svg`} width = "80%" alt="weather"/>  
                            </Grid>
                            <Grid item xs={6} textAlign="left" style={{fontSize:"40px", color:"#365a7a"}}>
                                <p style={{margin:"0px"}}>Today</p>
                                <h2 style={{margin:"0px"}}>{ weatherDataOnCityForm.cod === "200"? weatherDataOnCityForm.city.name : weatherData.city.name}</h2>
                                <p style={{margin:"0px"}}>Temperature: {weatherDataOnCityForm.cod === "200"? weatherDataOnCityForm.list[0].main.temp : weatherData.list[0].main.temp}°C </p>
                                <p style={{margin:"0px"}}>{weatherDataOnCityForm.cod === "200"? weatherDataOnCityForm.list[0].weather[0].main : weatherData.list[0].weather[0].main}</p>
                            </Grid>
                            <Grid sx={{ display: "flex",
                                        margin:"auto",
                                        marginBottom:"-11%",
                                        justifyContent:"center"
                                    }} >
                                { weatherDataOnCityForm.cod ==="200"?  
                                weatherDataOnCityForm.list.filter(data => data.dt_txt.includes("12:00:00")).slice(0,-1).map((value, index)=>{
                                    var day = moment(value.dt_txt).format("dddd");
                                    return(
                                            <Box sx={{ width: "19%",
                                                minHeight: "50px",
                                                boxShadow: "0px 0px 5px 9px #aed0eb",
                                                borderRadius: "35px",
                                                margin:"15px",
                                                }} key={index}>
                                    
                                            <Grid xs={12} md={12} sm={12} lg={12}
                                                container
                                                direction="row"
                                                justifyContent="center"
                                                alignItems="center"
                                                textAlign="center"
                                                > 
                                                <Grid item xs={6} textAlign="center" style={{ color:"#365a7a"}}>
                                                    <h2 style={{margin:"0px"}}>{day}</h2>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <img src={`images/${value.weather[0].icon}.svg`} width = "50%" alt="weather"/> 
                                                </Grid>
                                                <Grid item xs={6} textAlign="center" style={{fontSize:"30px", color:"#365a7a"}}>
                                                    <p style={{margin:"0px"}}>{value.main.temp}°C </p>
                                                    <p style={{margin:"0px"}}>{value.weather[0].main}</p>
                                                </Grid>
                                            </Grid>
                                        </Box>
                                    )
                                })
                            :
                                weatherData.list.filter(data => data.dt_txt.includes("12:00:00")).slice(0,-1).map((value, index)=>{
                                    var day = moment(value.dt_txt).format("dddd");
                                    return(
                                        
                                        <Box sx={{ width: "19%",
                                            minHeight: "50px",
                                            boxShadow: "0px 0px 5px 9px #aed0eb",
                                            borderRadius: "35px",
                                            margin:"15px"
                                            }} key={index}>
                                            <Grid xs={12} md={12} sm={12} lg={12}
                                                container
                                                direction="row"
                                                justifyContent="center"
                                                alignItems="center"
                                                textAlign="center"
                                                > 
                                                <Grid item xs={6} textAlign="center" style={{ color:"#365a7a"}}>
                                                    <h2 style={{margin:"0px"}}>{day}</h2>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <img src={`images/${value.weather[0].icon}.svg`} width = "50%" alt="weather"/> 
                                                </Grid>
                                                <Grid item xs={6} textAlign="center" style={{fontSize:"30px", color:"#365a7a"}}>
                                                    <p style={{margin:"0px"}}>{value.main.temp}°C </p>
                                                    <p style={{margin:"0px"}}>{value.weather[0].main}</p>
                                                </Grid>
                                            </Grid>
                                        </Box>
                                    )
                                })
                            }   
                        </Grid>                      
                    </Grid>
                </Box>
            : null}
        </Container>
    )
}



