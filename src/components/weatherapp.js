import { Grid, TextField } from "@mui/material"
import { Box, Container } from "@mui/system";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchWeather, getInputValue, onEnterSubmit } from "../actions/weather.action";

export const WeatherApp = () =>{
    const [status, setStatus] = useState(false);
    
    const onChangeHandler = (e)=>{
        dispatch(getInputValue(e.target.value));
    }
    const dispatch = useDispatch();
    const {input, weatherData} = useSelector((reduxData) =>
        reduxData.weatherReducer
    )
    const onEnter =(e)=>{         
        dispatch(getInputValue(e.target.value));
        if(e.code === "Enter"){
            //lấy giá trị input sau đó in hoa chữ cái đầu và bỏ khoảng cách
            const upperCase = input.charAt(0).toUpperCase() + input.slice(1);
            const cityRemoveSpace = upperCase.replace(/\s/g,'');
            //method get weather                
                dispatch(fetchWeather(cityRemoveSpace));
                dispatch(onEnterSubmit(weatherData));
            }
    }
    return(
        <Container xs={12} md={12} sm={12} lg={12}>
            <Box sx={{ display: "flex",
                        margin:"auto",
                        marginTop:"20%",
                        width: "90%",
                        minHeight: "500px",
                        boxShadow: "0px 0px 5px 9px #aed0eb",
                        borderRadius: "35px"
                    }}>
                <Grid xs={12} md={12} sm={12} lg={12}
                    container
                    direction="row"
                    justifyContent="center"
                    alignItems="center"
                    textAlign="center"
                    > 
                        <img src="images/02d.svg" width = "30%" alt="weather"/> 
                        <h1 style={{fontSize:"60px", color:"#365a7a"}}>Weather Forecast</h1>
                    <Grid item xs={12} md={12} sm={12} lg={12} marginTop="-20%"> 
                        <TextField id="outlined-basic" label="Enter a city" variant="standard" 
                        InputLabelProps={{
                            style : { color : '#365a7a', marginTop:"-2px", marginLeft:"20px"}
                        }} 
                        InputProps={{ 
                            disableUnderline: true, style:{color:"#365a7a",paddingBottom:"10px", marginLeft:"20px"} 
                        }}
                        onKeyDown={onEnter} onChange={onChangeHandler} value={input}
                        style={{width:"50%",backgroundColor: "white", borderRadius: 20}} className="inputRounded"/>
                    </Grid>
                </Grid>
            </Box>
        </Container> 
    )
}