import { ENTER_HANDLER, FETCH_API_SUCCESS, FETCH_WEATHER_API, GET_INPUT_VALUE, SUBMIT_ON_WEATHER_CITY, FETCH_API_SUCCESS_SUBMIT_CITY } from "../constants/weather.constants";
const initialState = {
    input: "",
    weatherData: [],
    inputOnCityForm:"",
    weatherDataOnCityForm : []

}
export default function weatherReducer (state = initialState, action){
    switch (action.type) {
        case GET_INPUT_VALUE:
                state.input = action.data
            break;
        case FETCH_API_SUCCESS:
                state.weatherData = action.data
                if(state.weatherData.cod === "200"){
                    state.input = "";
                    state.inputOnCityForm ="";
                    break;
                }
                if(state.weatherData.cod === "404"){
                    state.input = "City not found. Try again";
                    state.inputOnCityForm ="City not found. Try again"
                    break;
                }
            break;
        case ENTER_HANDLER:
                state.weatherData = action.data;
            break;

        case SUBMIT_ON_WEATHER_CITY:
            state.inputOnCityForm = action.data;
            state.weatherDataOnCityForm = action.data;
            break;
        case FETCH_API_SUCCESS_SUBMIT_CITY:
            state.weatherDataOnCityForm = action.data
            if(state.weatherDataOnCityForm.cod === "200"){
                state.inputOnCityForm ="";
                state.weatherData = action.data;
                break;
            }
            if(state.weatherDataOnCityForm.cod === "404"){
                state.inputOnCityForm ="City not found. Try again"
                break;
            }
        break;
        default:
            break;
    }
    console.log(state)
    return {...state}
}